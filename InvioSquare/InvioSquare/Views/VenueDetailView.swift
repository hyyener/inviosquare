//
//  VenueDetailView.swift
//  InvioSquare
//
//  Created by Faruk Yavuz on 16.05.2019.
//  Copyright © 2019 Hakki Yener. All rights reserved.
//

import UIKit
import MapKit

class VenueDetailView: UIView {
    @IBOutlet weak var venueMapView: MKMapView!
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var noImageLbl: UILabel!
    
    class func instanceFromNib() -> UIView {
        
        return UINib(nibName: "VenueDetailView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func closeButtonTouched(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    public func setVenue(_ venue: Venue){
        venueMapView.mapType = MKMapType.standard
        
        // 2)
        let location = CLLocationCoordinate2D(latitude: venue.location?.lat ?? 0,longitude: venue.location?.lng ?? 0)
        
        // 3)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        venueMapView.setRegion(region, animated: true)
        
        // 4)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = venue.name
        //annotation.subtitle = "Ahmedabad"
        venueMapView.addAnnotation(annotation)
        
        if let prefix = venue.bestPhoto?.prefix, let suffix = venue.bestPhoto?.suffix {
            let imagelink = prefix + "original" + suffix
                        
            venueImageView.downloaded(from: imagelink, contentMode: .scaleAspectFill)
        }
        else {
            noImageLbl.alpha = 1
        }
        

        
    }
}
