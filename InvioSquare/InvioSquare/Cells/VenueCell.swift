//
//  VenueCell.swift
//  InvioSquare
//
//  Created by Faruk Yavuz on 16.05.2019.
//  Copyright © 2019 Hakki Yener. All rights reserved.
//

import UIKit

class VenueCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            self.setSelected(false, animated: true)
        }
    }
    func setVenue(venue: Venues) {
        titleLabel.text = venue.name
        locationLabel.text = venue.location?.address
    }
    
}
