/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Venue : Codable {
	let id : String?
	let name : String?
	let contact : Contact?
	let location : Location?
	let canonicalUrl : String?
	let categories : [Categories]?
	let verified : Bool?
	let stats : Stats?
	let likes : Likes?
	let dislike : Bool?
	let ok : Bool?
	let rating : Double?
	let ratingColor : String?
	let ratingSignals : Int?
	let beenHere : BeenHere?
	let specials : Specials?
	let photos : Photos?
	//let reasons : Reasons?
	//let hereNow : HereNow?
	let createdAt : Int?
	let tips : Tips?
	let shortUrl : String?
	let timeZone : String?
	let listed : Listed?
	let popular : Popular?
	let pageUpdates : PageUpdates?
	let inbox : Inbox?
	let attributes : Attributes?
	let bestPhoto : BestPhoto?
	let colors : Colors?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case contact = "contact"
		case location = "location"
		case canonicalUrl = "canonicalUrl"
		case categories = "categories"
		case verified = "verified"
		case stats = "stats"
		case likes = "likes"
		case dislike = "dislike"
		case ok = "ok"
		case rating = "rating"
		case ratingColor = "ratingColor"
		case ratingSignals = "ratingSignals"
		case beenHere = "beenHere"
		case specials = "specials"
		case photos = "photos"
		//case reasons = "reasons"
		//case hereNow = "hereNow"
		case createdAt = "createdAt"
		case tips = "tips"
		case shortUrl = "shortUrl"
		case timeZone = "timeZone"
		case listed = "listed"
		case popular = "popular"
		case pageUpdates = "pageUpdates"
		case inbox = "inbox"
		case attributes = "attributes"
		case bestPhoto = "bestPhoto"
		case colors = "colors"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		contact = try values.decodeIfPresent(Contact.self, forKey: .contact)
		location = try values.decodeIfPresent(Location.self, forKey: .location)
		canonicalUrl = try values.decodeIfPresent(String.self, forKey: .canonicalUrl)
		categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
		verified = try values.decodeIfPresent(Bool.self, forKey: .verified)
		stats = try values.decodeIfPresent(Stats.self, forKey: .stats)
		likes = try values.decodeIfPresent(Likes.self, forKey: .likes)
		dislike = try values.decodeIfPresent(Bool.self, forKey: .dislike)
		ok = try values.decodeIfPresent(Bool.self, forKey: .ok)
		rating = try values.decodeIfPresent(Double.self, forKey: .rating)
		ratingColor = try values.decodeIfPresent(String.self, forKey: .ratingColor)
		ratingSignals = try values.decodeIfPresent(Int.self, forKey: .ratingSignals)
		beenHere = try values.decodeIfPresent(BeenHere.self, forKey: .beenHere)
		specials = try values.decodeIfPresent(Specials.self, forKey: .specials)
		photos = try values.decodeIfPresent(Photos.self, forKey: .photos)
		//reasons = try values.decodeIfPresent(Reasons.self, forKey: .reasons)
		//hereNow = try values.decodeIfPresent(HereNow.self, forKey: .hereNow)
		createdAt = try values.decodeIfPresent(Int.self, forKey: .createdAt)
		tips = try values.decodeIfPresent(Tips.self, forKey: .tips)
		shortUrl = try values.decodeIfPresent(String.self, forKey: .shortUrl)
		timeZone = try values.decodeIfPresent(String.self, forKey: .timeZone)
		listed = try values.decodeIfPresent(Listed.self, forKey: .listed)
		popular = try values.decodeIfPresent(Popular.self, forKey: .popular)
		pageUpdates = try values.decodeIfPresent(PageUpdates.self, forKey: .pageUpdates)
		inbox = try values.decodeIfPresent(Inbox.self, forKey: .inbox)
		attributes = try values.decodeIfPresent(Attributes.self, forKey: .attributes)
		bestPhoto = try values.decodeIfPresent(BestPhoto.self, forKey: .bestPhoto)
		colors = try values.decodeIfPresent(Colors.self, forKey: .colors)
	}

}
