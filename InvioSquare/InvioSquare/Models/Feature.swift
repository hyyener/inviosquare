/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Feature : Codable {
	let cc : String?
	let name : String?
	let displayName : String?
	let matchedName : String?
	let highlightedName : String?
	let woeType : Int?
	let slug : String?
	let id : String?
	let longId : String?
	let geometry : Geometry?

	enum CodingKeys: String, CodingKey {

		case cc = "cc"
		case name = "name"
		case displayName = "displayName"
		case matchedName = "matchedName"
		case highlightedName = "highlightedName"
		case woeType = "woeType"
		case slug = "slug"
		case id = "id"
		case longId = "longId"
		case geometry = "geometry"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		cc = try values.decodeIfPresent(String.self, forKey: .cc)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
		matchedName = try values.decodeIfPresent(String.self, forKey: .matchedName)
		highlightedName = try values.decodeIfPresent(String.self, forKey: .highlightedName)
		woeType = try values.decodeIfPresent(Int.self, forKey: .woeType)
		slug = try values.decodeIfPresent(String.self, forKey: .slug)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		longId = try values.decodeIfPresent(String.self, forKey: .longId)
		geometry = try values.decodeIfPresent(Geometry.self, forKey: .geometry)
	}

}