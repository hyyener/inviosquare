//
//  Network.swift
//  InvioSquare
//
//  Created by Faruk Yavuz on 16.05.2019.
//  Copyright © 2019 Hakki Yener. All rights reserved.
//

import Foundation

public class Network {
    
    private static let client = FoursquareAPIClient.init(clientId: Constants.clientId, clientSecret: Constants.clientSecret)
    
    static func searchVenues(query: String?, completion: @escaping (_ json: [Venues]?, _ success: Bool) -> Void){
        var queryString = ""
        if ((query?.count ?? 00 > 0)) {
            queryString = query!
        }
        else {
            queryString = Constants.emptyQueryString
        }
        
        let parameter: [String: String] = [
            "near": queryString,
            "limit": "10",
            ];
        
        client.request(path: "venues/search", parameter: parameter) { result in
            switch result {
            case let .success(data):
                // parse the JSON data with NSJSONSerialization or Lib like SwiftyJson
                // e.g. {"meta":{"code":200},"notifications":[{"...
                
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(SearchVenuesBaseResponse.self, from: data)
                    completion(responseModel.response?.venues, true)
                } catch _ {
                    completion(nil, false)
                }
                
            case let .failure(error):
                
                completion(nil, false)
                
                // Error handling
                switch error {
                case let .connectionError(connectionError):
                    print(connectionError)
                case let .responseParseError(responseParseError):
                    print(responseParseError)   // e.g. JSON text did not start with array or object and option to allow fragments not set.
                case let .apiError(apiError):
                    print(apiError.errorType)   // e.g. endpoint_error
                    print(apiError.errorDetail) // e.g. The requested path does not exist.
                }
            }
        }
    }
    
    static func getVenueDetail(venueId: String, completion: @escaping (_ venue: Venue?, _ success: Bool) -> Void){
        
        let parameter: [String: String] = [:];
        
        client.request(path: "venues/\(venueId)", parameter: parameter) { result in
            switch result {
            case let .success(data):
                // parse the JSON data with NSJSONSerialization or Lib like SwiftyJson
                // e.g. {"meta":{"code":200},"notifications":[{"...
                
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(GetVenueDetailBaseResponse.self, from: data)
                    completion(responseModel.response?.venue, true)
                } catch _ {
                    completion(nil, false)
                }
                
            case let .failure(error):
                
                completion(nil, false)
                
                // Error handling
                switch error {
                case let .connectionError(connectionError):
                    print(connectionError)
                case let .responseParseError(responseParseError):
                    print(responseParseError)   // e.g. JSON text did not start with array or object and option to allow fragments not set.
                case let .apiError(apiError):
                    print(apiError.errorType)   // e.g. endpoint_error
                    print(apiError.errorDetail) // e.g. The requested path does not exist.
                }
            }
        }
    }
}
