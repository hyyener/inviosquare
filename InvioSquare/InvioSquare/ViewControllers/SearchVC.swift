//
//  ViewController.swift
//  InvioSquare
//
//  Created by Faruk Yavuz on 15.05.2019.
//  Copyright © 2019 Hakki Yener. All rights reserved.
//

import UIKit
import JGProgressHUD

class SearchVC: UIViewController {

    @IBOutlet weak var searchTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        searchTF.layer.borderWidth = 1
        searchTF.layer.borderColor = UIColor.init(red: 155.0 / 255.0, green: 155.0 / 255.0, blue: 155.0 / 255.0, alpha: 1).cgColor
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VenuesSeg" {
            let destVC = segue.destination as! VenuesVC
            if let list = sender as? [Venues] {
                destVC.dataSource = list
            }
        }
    }
    @IBAction func searchButtonTouched(_ sender: Any) {
        let hud = JGProgressHUD(style: .dark)
        hud.show(in: self.view)
        
        Network.searchVenues(query: searchTF.text) { (response, success) in
            hud.dismiss()
            if success == true, let list = response {
                //print(list);
                self.performSegue(withIdentifier: "VenuesSeg", sender: list)
            }
            else {
                let errorAlert = UIAlertController(title: "Hata", message: "Bir hata oluştu", preferredStyle: UIAlertController.Style.alert)
                
                errorAlert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: nil))
                
                self.present(errorAlert, animated: true, completion: nil)
            }

        }
    }
    
    
}


extension SearchVC: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-zıİüÜğĞöÖçÇşŞ ].*", options: [])
            if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                return false
            }
        }
        catch {
            print("ERROR")
        }
        return true
    }
}

