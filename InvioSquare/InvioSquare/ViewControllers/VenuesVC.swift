//
//  VenuesVC.swift
//  InvioSquare
//
//  Created by Faruk Yavuz on 16.05.2019.
//  Copyright © 2019 Hakki Yener. All rights reserved.
//

import UIKit
import PureLayout
import JGProgressHUD

class VenuesVC: UIViewController {

    @IBOutlet weak var venuesTV: UITableView!
    var dataSource : [Venues] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        venuesTV.register(UINib(nibName: "VenueCell", bundle: nil), forCellReuseIdentifier: "VenueCell")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VenuesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VenueCell")! as! VenueCell
        cell.setVenue(venue: dataSource[indexPath.row])
        
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let venue = dataSource[indexPath.row]
        
        if let venueIdentifier = venue.id {
            let hud = JGProgressHUD(style: .dark)
            hud.show(in: self.view)
            Network.getVenueDetail(venueId: venueIdentifier) { (response, success) in
                hud.dismiss()
                if success == true, let venue = response {
                    let detailView = VenueDetailView.instanceFromNib() as! VenueDetailView
                    self.view.addSubview(detailView)
                    detailView.autoPinEdgesToSuperviewEdges()
                    detailView.setVenue(venue)
                }
                else {
                    let errorAlert = UIAlertController(title: "Hata", message: "Bir hata oluştu", preferredStyle: UIAlertController.Style.alert)
                    
                    errorAlert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: nil))
                    
                    self.present(errorAlert, animated: true, completion: nil)
                }
            }
        }

    }
}
